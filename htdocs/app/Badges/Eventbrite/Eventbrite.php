<?php

/**
 * User: jamesmills
 * Date: 04/02/2014
 * Time: 23:24
 */

namespace Badges\Eventbrite;

use Guzzle\Http\Client as GuzzleClient;

class Eventbrite
{

    protected $endpoint = 'https://www.eventbrite.com/json';
    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    protected function call($method, $params)
    {

        $client = new GuzzleClient($this->endpoint . '/' . $method . '?access_token=' . $this->token . '&' . http_build_query($params));
        $client->setDefaultOption('headers', array('Authorization' => 'Bearer'));
        $body = $client->get()->send()->getBody();
        return json_decode($body);
    }

    public function events()
    {
        $params = array(
            'asc_or_desc' => 'desc'
        );

        return $this->call('user_list_events', $params);
    }

    public function attendees($id)
    {
        $params = array(
            'id' => $id
        );

        return $this->call('event_list_attendees', $params);
    }
}
