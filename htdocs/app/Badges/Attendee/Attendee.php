<?php

/**
 * User: jamesmills
 * Date: 05/02/2014
 * Time: 19:28
 */

namespace Badges\Attendee;

class Attendee
{
    protected $name;
    protected $email;
    protected $company;
    protected $twitter;
    protected $avatar;

    public function ___construct($attendee)
    {
        foreach ($attendee as $property => $value) {
            $method = 'set' . ucfirst($property);
            if (method_exists($track, $method)) {
                call_user_func(array($track, $method), $value);
            }
        }
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $twitter
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {
        if (empty($this->twitter)) {
            return false;
        }

        if ($this->twitter[0] != '@') {
            $this->twitter = '@' . $this->twitter;
        }
        return $this->twitter;
    }


}
