<?php

namespace Badges\Attendee;

use Badges\Attendee\Attendee;
use Badges\Helpers\AvatarHelper;

class AttendeeFactory
{
    public static function fromEventBrite($entry)
    {

        $attendee = new Attendee;
        $attendee->setName($entry->attendee->first_name . ' ' . $entry->attendee->last_name);
        $attendee->setEmail($entry->attendee->email);
        if (isset($entry->attendee->company)) {
            $attendee->setCompany($entry->attendee->company);
        }
        if ($entry->attendee->answers) {
            $attendee->setTwitter($entry->attendee->answers[0]->answer->answer_text);
        }

        $avatar = new AvatarHelper($attendee);
        $attendee->setAvatar($avatar->get());

        return $attendee;
    }
}
