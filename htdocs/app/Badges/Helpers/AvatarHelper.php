<?php

/**
 * User: jamesmills
 * Date: 05/02/2014
 * Time: 20:30
 */

namespace Badges\Helpers;

use Badges\Attendee\Attendee;
use Guzzle\Http\Client;
use Guzzle\Plugin\Oauth\OauthPlugin;
use Symfony\Component\HttpKernel\Tests\Controller\ControllerResolverTest;

class AvatarHelper
{
    protected $email;

    public function __construct(Attendee $attendee)
    {
        $this->email = $attendee->getEmail();
        $this->twitter = $attendee->getTwitter();
    }

    public function get()
    {
        if ($this->find()) {
            return \Cache::get('avatar_' . md5($this->email));
        }

        return 'blank.gif';
    }

    public function find()
    {
        if (\Cache::has('avatar_' . md5($this->email))) {
            return true;
        } elseif ($this->tryGravatar()) {
            return true;
        } elseif ($this->tryTwitter()) {
            return true;
        }

        return false;
    }

    private function tryGravatar()
    {

        $data = @file_get_contents('http://www.gravatar.com/avatar/' . md5($this->email) . '.jpg?s=300&d=404');

        if ($data) {
            \Cache::forever('avatar_' . md5($this->email), md5($this->email) . '.jpg');
            file_put_contents(public_path() . '/avatars/' . md5($this->email) . '.jpg', $data);
            return true;
        }

        return false;
    }

    private function tryTwitter()
    {
        if (empty($this->twitter)) {
            return false;
        }


        $client = new Client('https://api.twitter.com/{version}', array('version' => '1.1'));
        $client->addSubscriber(new OauthPlugin(\Config::get('custom.twitter_oauth_settings')));
        $request = $client->get('users/show.json?screen_name=' . (string)$this->twitter);


        try {

            $response = $request->send()->json();

            if ($response['profile_image_url']) {

                $parts = explode('.', parse_url($response['profile_image_url'])['path']);
                $data = @file_get_contents($response['profile_image_url']);

                if (!isset($parts[1])) {
                    $ext = '';
                } else {
                    $ext = '.' . $parts[1];
                }

                file_put_contents(public_path() . '/avatars/' . md5($this->email) . $ext, $data);
                \Cache::forever('avatar_' . md5($this->email), md5($this->email) . $ext);
                return true;
            }

        } catch (\Guzzle\Http\Exception\ClientErrorResponseException $e) {

            if ($e->getCode() != 404) {

//                    $errors[] = $app->trans('Der Benutzername ist bereits vergeben.');

            }

        }


        return false;
    }
}
