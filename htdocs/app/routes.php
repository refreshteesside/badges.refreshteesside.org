<?php

Route::get('/test', function () {

        return 'test';
});

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController', ['only' => ['index', 'create', 'store', 'destroy']]);

Route::group(array('before' => 'auth'), function () {

    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('/events', ['as' => 'events', 'uses' => 'EventsController@index'])->before('connected');
    Route::get('/events/attendees', ['as' => 'attendees', 'uses' => 'EventsController@attendees'])->before('connected');

    Route::get('connector/eventbrite', 'ConnectorController@eventbrite');
    Route::get('connector/twitter', 'ConnectorController@twitter');

});


