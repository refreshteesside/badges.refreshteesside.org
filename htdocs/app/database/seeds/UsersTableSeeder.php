<?php

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->truncate();

        $users = array(
            'email' => 'james@jamesmills.co.uk',
            'first_name' => 'James',
            'last_name' => 'Mills',
            'company' => 'Refresh Teesside',
            'template' => 'avery_j8165',
            'colour_primary' => '7fc5c5',
            'logo' => '1.jpg',
            'password' => Hash::make('123')
        );

        DB::table('users')->insert($users);
    }
}
