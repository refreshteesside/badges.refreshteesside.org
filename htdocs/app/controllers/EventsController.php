<?php

use Badges\Eventbrite\Eventbrite;
use Badges\Attendee\AttendeeCollection;
use Badges\Attendee\Attendee;
use Badges\Attendee\AttendeeFactory;

class EventsController extends \BaseController
{

    public function index()
    {
        $eventbrite = new Eventbrite(Crypt::decrypt(Auth::user()->eventbrite_token));
        $results = $eventbrite->events();

        return View::make('events', array('events' => $results->events));
    }

    public function attendees()
    {

        $eventbrite = new Eventbrite(Crypt::decrypt(Auth::user()->eventbrite_token));
        $results = $eventbrite->attendees(Input::get('id'));

        $my_attendee_collection = new AttendeeCollection;

        foreach ($results->attendees as $attendee) {
            $my_attendee_collection[] = AttendeeFactory::fromEventBrite($attendee);
        }

        return View::make('namebadges.' . Auth::user()->template, array('attendees' => $my_attendee_collection));

    }
}
