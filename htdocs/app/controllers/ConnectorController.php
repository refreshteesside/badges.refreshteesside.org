<?php

class ConnectorController extends \BaseController {

    public function eventbrite()
    {
        $provider = new League\OAuth2\Client\Provider\Eventbrite(array(
            'clientId'  =>  'WONVODZK2E3L55JVFJ',
            'clientSecret'  =>  'KFIUU6ISQ5I5C7QEBEDCSYNWT7JHBNILZEEQLNVQDLJVDY54X5',
            'redirectUri'   =>  'http://badges.refreshteesside.org/connector/eventbrite'
        ));


        if ( ! isset($_GET['code'])) {

            // If we don't have an authorization code then get one
            $provider->authorize();

        } else {

            try {

                // Try to get an access token (using the authorization code grant)
                $t = $provider->getAccessToken('authorization_code', array('code' => $_GET['code'], 'grant_type' => 'authorization_code'));

                $user = User::find(Auth::user()->id);
                $user->eventbrite_token = Crypt::encrypt($t->accessToken);
                $user->save();

                Session::flash('success', 'Connected to eventbrite!');
                return Redirect::to('dashboard');

            } catch (Exception $e) {

                Session::flash('error', 'Failed to connect to Eventbrite. Sorry about that.');
                return Redirect::to('dashboard');


            }
        }
    }

    public function twitter()
    {
        $provider = new League\OAuth2\Client\Provider\Twitter(array(
            'clientId'  =>  '8ftQ78qsYMveS9xqIihg',
            'clientSecret'  =>  'ksq1rNGHLum9gxk32eh8spjZoXCBhWgLtM9sznBrdA',
            'redirectUri'   =>  'http://badges.refreshteesside.org/connector/twitter'
        ));


        if ( ! isset($_GET['code'])) {

            // If we don't have an authorization code then get one
            $provider->authorize();

        } else {

            try {

                // Try to get an access token (using the authorization code grant)
                $t = $provider->getAccessToken('authorization_code', array('code' => $_GET['code'], 'grant_type' => 'client_credentials'));

                $user = User::find(Auth::user()->id);
                $user->twitter_token = Crypt::encrypt($t->accessToken);
                $user->save();

                Session::flash('success', 'Connected to Twitter!');
                return Redirect::home();

            } catch (Exception $e) {

                Session::flash('error', 'Failed to connect to Twitter. Sorry about that.');
                return Redirect::home();


            }
        }
    }
}