<?php

class SessionsController extends BaseController
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::check())
        {
            Session::flash('success', 'You are already logged in');
            return Redirect::to('dashboard');
        }
        return View::make('sessions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        // validate


        $input = Input::all();

        $attempt = Auth::attempt(array('email' => $input['email'], 'password' => $input['password']));

        if ($attempt) {
            Session::flash('success', 'You have been logged in');
            return Redirect::intended('/dashboard');
        }

        Session::flash('error', 'Invalid credentials');
        return Redirect::back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        Auth::logout();
        return Redirect::home();
    }

}
