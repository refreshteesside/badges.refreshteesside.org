
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Custom name badges for your event using attendee information from Eventbrite.">
    <meta name="author" content="">

    <title>Eventbrite Name Badges</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="inner cover">
                <h1 class="cover-heading">Eventbrite Name Badges</h1>
                <p class="lead">Custom name badges for your event using attendee information from Eventbrite. It's that simple.</p>
                <div>
                    <img src="/assets/img/avery_j8165_example.png" alt="Example"/>
                </div>
                <br>
                <p class="lead">
                    <a href="/login" class="btn btn-lg btn-default">Login</a>
                </p>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Name badges for <a href="http://refreshteesside.org">Refresh Teesside</a>, by <a href="https://twitter.com/jamesmills">@jamesmills</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>-->
<!--<script src="../../dist/js/bootstrap.min.js"></script>-->
<!--<script src="../../assets/js/docs.min.js"></script>-->
</body>
</html>