<?php require_once '_partials/head.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../assets/css/namebadges.css">
        <title>Eventbrite Name Badges</title>
    </head>
    <body>

    <div id="wrapper">
        <?php foreach($attendees as $attendee): ?>
            <div class="attendee">
                <img class="img_bg" src="../assets/main_bg.jpg">
                <img class="twitter_img" src="<?php echo $attendee->getAvatar(); ?>" />
                <div class="info_holder">
                    <h2><?php echo $attendee->getName(); ?></h2>
                    <p class="company"><?php echo $attendee->getCompany(); ?></p>
                    <?php if($attendee->hasTwitter()): ?>
                        <p class="twitter_name"><?php echo $attendee->getTwitterUsername(); ?></p>
                    <?php endif; ?>
                </div><!-- /.info_holder -->
            </div><!-- /.attendee -->
        <?php endforeach; ?>
    </div><!-- /.wrapper -->

    </body>
</html>