<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/assets/css/namebadges/avery_j8165.css">
    <script src="/assets/js/jquery-2.0.3.min.js"></script>
    <script src="/assets/js/namebadges/avery_j8165.js"></script>
    <title>Eventbrite Name Badges</title>
</head>
<body>

<div id="wrapper">

    <div class="page-topper"></div>

    <?php $i = 0; ?>

    <?php foreach ($attendees as $attendee): ?>

    <div class="attendee">

        <div class="info_holder">
            <div class="name">
                <span style="color: #{{ Auth::user()->colour_primary }};">
                    {{$attendee->getName() }}
                </span>
            </div>

            <div class="company">
                <span>
                        {{ $attendee->getCompany() }}
                </span>
            </div>
        </div><!-- /.info_holder -->

        <img style="border: 10px solid #{{ Auth::user()->colour_primary }};" class="avatar" src="/avatars/{{ $attendee->getAvatar() }}"/>

        <div class="bottom-right">
            <p class="twitter_name">{{ $attendee->getTwitter() }}</p>
            <img class="logo" src="/logos/{{ Auth::user()->logo }}">
        </div>

        <div class="powered">
            <p>Printed using badges.refreshteesside.org</p>
        </div>


    </div><!-- /.attendee -->

    <?php
        if ($i == 7) {

            echo '<div class="page-break">&nbsp;</div>';
            echo '<div class="page-topper"></div>';
            $i = 0;

        } else {

            $i++;

        }
    ?>

    <?php endforeach; ?>
</div><!-- /.wrapper -->

</body>
</html>