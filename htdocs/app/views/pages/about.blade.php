<h2>The Story</h2>

<p>
    Have you ever gone to an event where they give you a thick marker pen and a stick address lable and expect you to
    write your own name badge? Yer, so have we. It's so unprofessional and not at all what we wanted for the Refresh Teesside events.
</p>

<p>
    We wanted to be able to great people and present them with a customised name badge with their own personal information already printed and
    we even wanted to include something really unique, a profile picture. We looked at using the option in Eventbrite but they just did not fit
    what we wanted, we wanted something really unique.
</p>

<p>
    Luckily James, our founder, is a developer and so he quickly come up with a way of using the information from Eventbrite attendee feed
</p>