@if (Auth::user()->eventbrite_token)
    <button type="button" class="btn btn-success btn-lg btn-block" disabled="disabled">
        Connected to Eventbrite <span class="glyphicon glyphicon-ok"></span>
    </button>
    @else
    <a class="btn btn-danger btn-lg btn-block" href="/connector/eventbrite">Connect to Eventbrite</a>
@endif

@if (Auth::user()->eventbrite_token)
    <a class="btn btn-primary btn-lg btn-block" href="/events">Select event</a>
@endif

<hr>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Your logo</h3>
    </div>
    <div class="panel-body" style="text-align: center">
        <img class="thumbnail logo" src="/logos/{{ Auth::user()->logo }}">
        <br>
        <div class="colour_primary" style="background-color: #{{ Auth::user()->colour_primary }};"></div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Settings</h3>
    </div>
    <div class="panel-body">
        <p>
            All accounts are currenty set to use the template of <strong>Avery J8165</strong>. You can get these from
            most office supplies stores including <a
                href="http://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Doffice-products&field-keywords=Avery%20J8165">Amazon</a>.
        </p>
    </div>
</div>

<hr>

<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">Important for when printing</h3>
    </div>
    <div class="panel-body">
        <p>
            You will be presented with a page in your web browser which you can send to the printer.
        </p>
        <p>
            This has only been tested with Chrome.
        </p>
        <p>
            You must make sure that
        </p>
        <ol>
            <li>You have selected portrait</li>
            <li>You have changed margins to none</li>
            <li>If you have the option untick headers and footers</li>
        </ol>

        <img class="well" src="/assets/img/screen_shot.png"/>
    </div>
</div>