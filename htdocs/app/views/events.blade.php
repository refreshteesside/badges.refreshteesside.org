@extends('layouts.master')

@section('title')
HOME
@stop

@section('content')

<div class="row">

    <div class="col-md-8">

        <h1>Your events</h1>

        @if(!empty($events))

            <table class="table table-striped table-bordered">
                <tr>
                    <th>Events</th>
                    <th width="100">Print</th>
                </tr>

                @foreach ($events as $event)

                <tr>
                    <td>
                        <strong>{{ $event->event->title }}</strong> <br>
                        <span class="label label-info">{{ $event->event->status }}</span>
                        <span class="label label-default">{{ date('D jS M y g:ia', strtotime($event->event->start_date)) }}</span>
                        <span class="label label-default">{{ date('D jS M y g:ia', strtotime($event->event->end_date))  }}</span>

                        @if(isset($event->event->tickets))
                        <br>Tickets: (Sold: {{ $event->event->tickets[0]->ticket->quantity_sold }}) (Available: {{ $event->event->tickets[0]->ticket->quantity_available }})
                        @endif

                    </td>
                    <td style="vertical-align:middle;">
                        <a class="btn btn-primary" href="/events/attendees?id={{ $event->event->id }}">Print badges for event</a>
                    </td>
                </tr>

                @endforeach

            </table>

        @else

            <p>You have no events</p>

        @endif

    </div>

    <div class="col-md-4">

        @include('partials.side')

    </div>

</div>

@stop