<!DOCTYPE html>
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="en" charset="utf-8"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="en" charset="utf-8"><![endif]-->
<!--[!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>
        @section('title')
            Name Bdges
        @show
    </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css" media="screen">
    <style type="text/css">
        body {
            padding-top: 20px;
        }
    </style>
    <link rel="stylesheet" href="/assets/css/custom.css" media="screen">

    <script src="/assets/js/jquery-2.0.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</head>
<body lang="en">

    <div class="container">

        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Eventbrite Name Badges</a>
                </div>

                @if (Auth::check())
                <p class="navbar-text">
                    Signed in as {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                    <strong>{{ Auth::user()->company }}</strong>
                    <a href="/logout">Logout</a>
                </p>
                @endif

            </div>
        </nav>

        <div style="margin-top:18px;">
            @include('notifications')
        </div>

        <div style="margin-top:18px;">
            {{ Session::get('flash_message') }}
        </div>

        @yield('content')

    </div>

</body>
</html>