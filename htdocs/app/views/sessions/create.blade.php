@extends('layouts.master')

@section('title')
Login
@stop

@section('content')


{{ Form::open(array('route' => 'sessions.store', 'role' => 'form', 'class' => 'form-signin')) }}

<h2 class="form-signin-heading">Please sign in</h2>
{{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email address', 'required', 'autofocus')) }}

{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required')) }}

<label class="checkbox">
    <input type="checkbox" value="remember-me"> Remember me
</label>

<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

{{ Form::close() }}






@stop

